package com.jadeja.contacts.contact_list;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jadeja.contacts.R;
import com.jadeja.contacts.contact_detail.ContactDetailActivity;
import com.jadeja.contacts.models.Contact;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


/**
 * <h1>ContactAdapter</h1>
 * Holds list of contact data
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 28 July 2019
 */
public class ContactAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Contact> contacts;
    private Context context;

    ContactAdapter(Context context, List<Contact> contacts) {
        this.context = context;
        this.contacts = contacts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact contact = contacts.get(position);

        //set data to view
        holder.name.setText(contact.getName());
        holder.tvNumber.setText(contact.getMobileNumber());
        Glide.with(context).load(contact.getPhotoURI()).placeholder(R.drawable.ic_user).apply(RequestOptions.circleCropTransform()).into(holder.photo);

        /*passing data and redirect to detail screen*/
        holder.itemView.setOnClickListener(v -> context.startActivity(new Intent(context, ContactDetailActivity.class)
                .putExtra("photo", contact.getPhotoURI().toString())
                .putExtra("name", contact.getName())
                .putExtra("number", contact.getMobileNumber())
                .putExtra("email", contact.getEmail())
        ));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    //updates the data
    void updateContacts(List<Contact> contacts) {
        this.contacts = contacts;
        notifyDataSetChanged();
    }
}
