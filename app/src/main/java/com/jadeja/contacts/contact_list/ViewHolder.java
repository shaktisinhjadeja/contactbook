package com.jadeja.contacts.contact_list;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jadeja.contacts.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h1>ViewHolder</h1>
 * Holds list items views
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 28 July 2019
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivImage)
    ImageView photo;
    @BindView(R.id.tvName)
    TextView name;
    @BindView(R.id.tvNumber)
    TextView tvNumber;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
