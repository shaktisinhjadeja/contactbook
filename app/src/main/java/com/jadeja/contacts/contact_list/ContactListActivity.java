package com.jadeja.contacts.contact_list;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.Window;

import com.jadeja.contacts.R;
import com.jadeja.contacts.models.Contact;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h1>ContactListActivity</h1>
 * Fetches the contacts and shows it.
 *
 * @author Shaktisinh Jaeja
 * @version 1.0
 * @since 28 July 2019
 */

public class ContactListActivity extends AppCompatActivity {

    private static final int ACCESS_CONTACT_PERMISSION = 135;
    private static final int PERMISSION_FROM_SETTINGS = 246;

    @BindView(R.id.rvContacts)
    RecyclerView rvContacts;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ContactAdapter adapter;
    private List<Contact> contacts;
    private Dialog progress;
    private AlertDialog.Builder permissionDeniedAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        toolbarSetup();

        contacts = new ArrayList<>();
        adapter = new ContactAdapter(this, contacts);
        rvContacts.setLayoutManager(new LinearLayoutManager(this));
        rvContacts.addItemDecoration(new DividerItemDecoration(rvContacts.getContext(), DividerItemDecoration.VERTICAL));
        rvContacts.setAdapter(adapter);

        progress = new Dialog(this);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.setContentView(R.layout.dialog_progress);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        permissionDeniedAlert = new AlertDialog.Builder(this);
        permissionDeniedAlert.setTitle(R.string.permission_denied);
        permissionDeniedAlert.setMessage(R.string.permission_denied_message);
        permissionDeniedAlert.setPositiveButton(R.string.ok, (dialog, which) -> checkPermission());
        permissionDeniedAlert.setNeutralButton(R.string.go_to_settings, (dialog, which) -> gotoSettings());

        checkPermission();
    }

    //setups toolbar
    private void toolbarSetup() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.app_name));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ACCESS_CONTACT_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                fetchContacts();
            } else {
                hideProgress();
                permissionDeniedAlert.show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PERMISSION_FROM_SETTINGS) {
            checkPermission();
        }
    }

    /**
     * Checks permissions to read contact, send sms and do call. if permission is not there it asks for that
     */
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS}, ACCESS_CONTACT_PERMISSION);
        } else {
            fetchContacts();
        }
    }

    //get contact and update it to the adapter
    private void fetchContacts() {
        showProgress();
        contacts = getContacts(this);
        adapter.updateContacts(contacts);
    }

    //redirects to setting to allow permission
    private void gotoSettings() {
        Intent intent = new Intent();
        intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, PERMISSION_FROM_SETTINGS);
    }

    /**
     * Fetches the contact details
     * @param ctx : Context
     */
    public List<Contact> getContacts(Context ctx) {
        List<Contact> list = new ArrayList<>();
        ContentResolver contentResolver = ctx.getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        assert cursor != null;
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor cursorInfo = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(ctx.getContentResolver(),
                            ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id)));

                    Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id));
                    Uri pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

                    Bitmap photo = null;
                    if (inputStream != null) {
                        photo = BitmapFactory.decodeStream(inputStream);
                    }
                    assert cursorInfo != null;
                    while (cursorInfo.moveToNext()) {
                        Contact info = new Contact();
                        info.setId(id);
                        info.setName(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                        info.setMobileNumber(cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                        info.setPhoto(photo);
                        info.setPhotoURI(pURI);
                        list.add(info);
                    }

                    cursorInfo.close();
                }
            }
            cursor.close();
        }

        hideProgress();
        rvContacts.setVisibility(View.VISIBLE);
        return list;
    }

    //display progress
    private void showProgress() {
        if (progress != null && progress.isShowing())
            progress.show();
    }

    //hide progress
    private void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }
}
