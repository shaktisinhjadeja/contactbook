package com.jadeja.contacts.models;


import android.graphics.Bitmap;
import android.net.Uri;

import java.io.Serializable;

/**
 * <h1>Contact</h1>
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 28 July 2019
 */
public class Contact implements Serializable {
    private String id;
    private String name;
    private String email;
    private String mobileNumber;
    private Bitmap photo;
    private Uri photoURI;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public Uri getPhotoURI() {
        return photoURI;
    }

    public void setPhotoURI(Uri photoURI) {
        this.photoURI = photoURI;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
