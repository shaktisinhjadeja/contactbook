package com.jadeja.contacts.contact_detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jadeja.contacts.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * <h1>ContactDetailActivity</h1>
 * Displays contact details and can do call and send sms
 *
 * @author Shaktisinh Jaeja
 * @version 1.0
 * @since 28 July 2019
 */

public class ContactDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvNumber)
    TextView tvNumber;
    @BindView(R.id.ivImage)
    ImageView ivImage;

    private String photo;
    private String name;
    private String mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        ButterKnife.bind(this);

        photo = getIntent().getStringExtra("photo");
        name = getIntent().getStringExtra("name");
        mobileNumber = getIntent().getStringExtra("number");

        toolbarSetup();
        tvNumber.setText(mobileNumber);
        Glide.with(this).load(Uri.parse(photo)).placeholder(R.drawable.ic_user).apply(RequestOptions.circleCropTransform()).into(ivImage);
    }

    //call
    @OnClick(R.id.ibCall)
    public void call() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + mobileNumber));
        startActivity(callIntent);
    }

    //send sms
    @OnClick(R.id.ibSms)
    public void sms() {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + mobileNumber));
        startActivity(sendIntent);
    }

    //setup toolbar
    private void toolbarSetup() {
        toolbar.setTitle(name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
